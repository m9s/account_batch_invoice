#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Not, In, Bool, Get, If, And, Or
from trytond.modules.account_batch import BATCH_LINE_STATES, BATCH_LINE_DEPENDS
from trytond.transaction import Transaction
from trytond.pool import Pool


_ZERO = Decimal("0.0")


class Line(ModelSQL, ModelView):
    'Account Batch Entry Line'
    _name = 'account.batch.line'

    invoice = fields.Many2One('account.invoice', 'Invoice',
            domain=[
                ('party', If(Bool(Eval('party')), '=', '!='), Eval('party')),
                ('state', 'in',
                    If(
                        In(Eval('state'), ['new', 'draft']),
                        ['open', ], ['open', 'paid']
                    )
                ),
            ], states={
                'readonly':
                    Or(In(Get(Eval('_parent_batch', {}), 'journal_type'),
                        ['revenue', 'expense']),
                        And(Not(In(Get(Eval('_parent_batch', {}),
                            'journal_type'),['revenue', 'expense'])),
                            Bool(BATCH_LINE_STATES['readonly'])))
            }, on_change=['invoice', 'date', 'journal', 'amount'],
            depends=BATCH_LINE_DEPENDS+['state', 'party'])

    def __init__(self):
        super(Line, self).__init__()
        self.party = copy.copy(self.party)
        if self.party.on_change is None:
            self.party.on_change = ['party', 'invoice', 'date']
        elif 'party' not in self.party.on_change:
            self.party.on_change += ['party']
        elif 'invoice' not in self.party.on_change:
            self.party.on_change += ['invoice']
        elif 'date' not in self.party.on_change:
            self.party.on_change += ['date']

        self.account = copy.copy(self.account)
        if self.account.on_change is None:
            self.account.on_change = ['account', 'invoice']
        elif 'invoice' not in self.account.on_change:
            self.account.on_change += ['invoice']
        elif 'account' not in self.account.on_change:
            self.account.on_change += ['account']

        self.is_cancelation_move = copy.copy(self.is_cancelation_move)
        if self.is_cancelation_move.states is None:
            self.is_cancelation_move.states = {
                    'readonly': Bool(Eval('invoice'))
                    }
        elif 'readonly' in self.is_cancelation_move.states:
            self.is_cancelation_move.states['readonly'] = \
                    self.is_cancelation_move.states['readonly'] or \
                    Bool(Eval('invoice'))
        else:
            self.is_cancelation_move.states['readonly'] = Bool(Eval('invoice'))

        self._reset_columns()

        self._error_messages.update({
                'amount_greater_invoice_amount_to_pay': 'Amount %s '
                    'greater than the amount to pay of invoice!\n'
                    '(Invoice: %s, Seq.: %s)',
            })

    def on_change_invoice(self, vals):
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        journal_obj = pool.get('account.batch.journal')
        currency_obj = pool.get('currency.currency')
        res = {}
        date = vals.get('date', None)
        amount = vals.get('amount')
        if vals.get('invoice'):
            res['is_cancelation_move'] = False
            invoice = invoice_obj.browse(vals['invoice'])
            journal = journal_obj.browse(vals['journal'])
            with Transaction().set_context(date=invoice.currency_date):
                amount_to_pay = currency_obj.compute(invoice.currency.id,
                    invoice.amount_to_pay, journal.currency.id)
            if not amount or abs(amount) > amount_to_pay:
                res['amount'] = amount_to_pay
            if invoice.type in ['in_credit_note', 'out_credit_note']:
                if res.get('amount'):
                    res['amount'] = res['amount'] * -1
                else:
                    res['amount'] = amount * -1
            res['party'] = invoice.party.id
            res['contra_account'] = self._get_invoice_account_id(
                    invoice.account.id, date)
            if invoice.type in ['out_invoice', 'out_credit_note']:
                res['external_reference'] = invoice.number
            elif invoice.type in ['in_invoice', 'in_credit_note']:
                res['external_reference'] = invoice.external_reference
        else:
            res['external_reference'] = False
        return res

    def on_change_party(self, value):
        invoice_obj = Pool().get('account.invoice')

        res = super(Line, self).on_change_party(value)

        party_id = value.get('party')
        invoice_id = value.get('invoice')
        if invoice_id:
            invoice = invoice_obj.browse(invoice_id)
            if party_id and party_id != invoice.party.id:
                res['invoice'] = False
                res['contra_account'] = False
                res['external_reference'] = False
            elif not party_id:
                res['invoice'] = False
                res['external_reference'] = False
            else:
                date = value.get('date', None)
                res['contra_account'] = self._get_invoice_account_id(
                        invoice.account.id, date)
        return res

    def on_change_account(self, value):
        invoice_obj = Pool().get('account.invoice')
        res = super(Line, self).on_change_account(value)
        account = value.get('account')
        invoice_id = value.get('invoice')
        if invoice_id:
            invoice = invoice_obj.browse(invoice_id)
            if account:
                if invoice.account.id != account:
                    res['invoice'] = False
            else:
                res['account'] = invoice.account.id
        return res

    def _check_invoice_amount(self, line):
        lang_obj = Pool().get('ir.lang')
        currency_obj = Pool().get('currency.currency')
        invoice_id = line.invoice.id
        amount_payable = self._get_amount_payable(invoice_id)[invoice_id]
        with Transaction().set_context(date=line.invoice.currency_date):
            amount_payable = currency_obj.compute(line.invoice.currency.id,
                amount_payable, line.journal.currency.id)
        currency_digits = line.journal.currency.digits
        line_amount = line.amount

        if amount_payable < _ZERO:
            lang_ids = lang_obj.search([
                        ('code', '=',
                            Transaction().context.get('language', 'en_US')),
                    ], limit=1)
            lang = lang_obj.browse(lang_ids[0])
            amount = lang_obj.format(lang,
                    '%.' + str(currency_digits) + 'f',
                    line_amount, True)
            self.raise_user_error('amount_greater_invoice_amount_to_pay',
                    error_args=(amount, line.invoice.number, line.sequence))
        return True

    def _get_amount_payable(self, ids):
        pool = Pool()
        currency_obj = pool.get('currency.currency')
        invoice_obj = pool.get('account.invoice')

        if isinstance(ids, (int, long)):
            ids = [ids]
        open_invoice_ids = invoice_obj.search([
            ('id', 'in', ids),
            ('state', '=', 'open'),
            ])

        res = dict((x, _ZERO) for x in ids)
        for invoice in invoice_obj.browse(open_invoice_ids):
            amount = _ZERO
            amount_currency = _ZERO
            for line in invoice.lines_to_pay:
                if line.second_currency.id == invoice.currency.id:
                    if line.debit - line.credit > _ZERO:
                        amount_currency += abs(line.amount_second_currency)
                    else:
                        amount_currency -= abs(line.amount_second_currency)
                else:
                    amount += line.debit - line.credit
            for line in invoice.payment_lines:
                if line.second_currency.id == invoice.currency.id:
                    if line.debit - line.credit > _ZERO:
                        amount_currency += abs(line.amount_second_currency)
                    else:
                        amount_currency -= abs(line.amount_second_currency)
                else:
                    amount += line.debit - line.credit
            if invoice.type in ('in_invoice', 'out_credit_note'):
                amount = - amount
                amount_currency = - amount_currency
            if amount != _ZERO:
                with Transaction().set_context(date=invoice.currency_date):
                    amount_currency += currency_obj.compute(
                    invoice.company.currency.id, amount, invoice.currency.id)
            res[invoice.id] = amount_currency
        return res

    def post(self, ids):
        pool = Pool()
        move_obj = pool.get('account.move')
        move_line_obj = pool.get('account.move.line')
        invoice_obj = pool.get('account.invoice')
        if isinstance(ids, (int, long)):
            ids = [ids]

        super(Line, self).post(ids)

        lines = self.browse(ids)
        for line in lines:
            if line.invoice:
                move_id = line.move.id
                amount_to_pay = line.invoice.amount_to_pay
                move = move_obj.browse(move_id)
                line_id = None
                for move_line in move.lines:
                    if move_line.account.id == self._get_invoice_account_id(
                            line.invoice.account.id, line.date):
                        line_id = move_line.id
                        break
                # Don't reconcile here, if there are other draft lines
                # refering to this invoice!
                draft_batch_lines = self.search([
                    ('invoice', '=', line.invoice),
                    ('id', '!=', line_id),
                    ('state', '=', 'draft'),
                    ])
                if amount_to_pay == _ZERO and not draft_batch_lines:
                    reconcile_lines = invoice_obj.get_reconcile_lines_for_amount(
                            line.invoice, _ZERO, exclude_ids=[line_id])
                    if reconcile_lines[1] == Decimal('0.0'):
                        line_ids = reconcile_lines[0] + [line_id]
                        move_line_obj.reconcile(line_ids)

    def _get_invoice_account_id(self, account_id, date):
        return account_id

    def create(self, vals):

        batch_line_id = super(Line, self).create(vals)

        line = self.browse(batch_line_id)
        if line.invoice:
            self._add_payment_line(line)
        return batch_line_id

    def write(self, ids, vals):
        if isinstance(ids, (int, long)):
            ids = [ids]

        res = super(Line, self).write(ids, vals)

        batch_lines = self.browse(ids)
        for line in batch_lines:
            if not line.invoice:
                continue
            self._add_payment_line(line)
            # This check must be here *after* payment lines are added, because
            # it takes into account the payment lines generated above
            self._check_invoice_amount(line)
        return res

    def _add_payment_line(self, line):
        move_obj = Pool().get('account.move')
        invoice_obj = Pool().get('account.invoice')
        move = move_obj.browse(line.move.id)
        for move_line in move.lines:
            if move_line.account.id == self._get_invoice_account_id(
                    line.invoice.account.id, line.date):
                invoice_obj.write(line.invoice.id, {
                        'payment_lines': [('add', move_line.id)],
                        })
                break

    def copy(self, ids, default=None):
        if default is None:
            default = {}

        default = default.copy()
        default['invoice'] = False

        return super(Line, self).copy(ids, default=default)

Line()
