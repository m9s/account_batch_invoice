#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Batch Invoice',
    'name_de_DE': 'Buchhaltung Buchungsstapel Rechnung',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Invoices on Batches
    - Provides the possibility to select invoices in batch entry lines.
    - Note:
      This module is a replacement for module account_statement. You should use
      either one or the other.
    ''',
    'description_de_DE': '''Rechnungen im Buchungsstapel
    - Ermöglicht die Auswahl von Rechnungen in der Stapelbuchungszeile.
    - Hinweis:
      Dieses Modul ist ein Ersatz für das Modul account_statement. Sie sollten
      nur eines von beiden benutzen.
    ''',
    'depends': [
        'account_batch',
        'account_invoice_external_reference',
    ],
    'xml': [
        'batch.xml',
        'invoice.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
